import XCTest

import UserInstructionTests

var tests = [XCTestCaseEntry]()
tests += UserInstructionTests.allTests()
XCTMain(tests)
