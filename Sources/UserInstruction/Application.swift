import CouchDB
import Foundation
import Kitura
import LoggerAPI

public class App {

  var client: CouchDBClient?
  var database: Database?

  let router = Router()

  private func postInit() {

    let connectionProperties = ConnectionProperties(host: "localhost",
                                                port: 5984,
                                                secured: false,
						username:"admin",
						password: "000000")
    client = CouchDBClient(connectionProperties: connectionProperties)

    client!.retrieveDB("instructions") { database, error in
      guard let database = database else {

        Log.info("Could not retrieve instructions database: "
        + "\(String(describing: error?.localizedDescription)) "
        + "- attempting to create new one.")
       self.createNewDatabase()
       return
     }

    Log.info("instructions database located - loading...")
    self.finalizeRoutes(with: database)
  }
  }

  private func createNewDatabase() {

client?.createDB("instructions") { database, error in

  guard let database = database else {
    Log.error("Could not create new database: "
      + "(\(String(describing: error?.localizedDescription))) "
      + "- instruction routes not created")
    return
  }
  self.finalizeRoutes(with: database)
}


  }

  private func finalizeRoutes(with database: Database) {
    // 5
self.database = database
initializeUserInstructionRoutes(app: self)
Log.info("instruction routes created")

  }

  public func run() {
    // 6
    postInit()
    Kitura.addHTTPServer(onPort: 8080, with: router)
    Kitura.run()
  }
}
