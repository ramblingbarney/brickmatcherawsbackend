import CouchDB

struct UserInstruction: Document {

  let _id: String?
  var _rev: String?
  var headline: String
  var description: String
}
