import CouchDB
import Kitura
import KituraContracts
import LoggerAPI

private var database: Database?

func initializeUserInstructionRoutes(app: App) {

  database = app.database
  app.router.get("/instructions", handler: getInstructions)
  app.router.post("/instructions", handler: addInstruction)
  app.router.delete("/instructions", handler: deleteInstruction)
}

private func getInstructions(completion: @escaping ([UserInstruction]?,
  RequestError?) -> Void) {

  guard let database = database else {
    return completion(nil, .internalServerError)
  }
  UserInstruction.Persistence.getAll(from: database) { instructions, error in
    return completion(instructions, error as? RequestError)
  }
}

private func addInstruction(instruction: UserInstruction, completion: @escaping (UserInstruction?,
  RequestError?) -> Void) {

  guard let database = database else {
    return completion(nil, .internalServerError)
  }
  UserInstruction.Persistence.save(instruction, to: database) { newInstruction, error in
    return completion(newInstruction, error as? RequestError)
  }
}

private func deleteInstruction(id: String, completion: @escaping
  (RequestError?) -> Void) {

  guard let database = database else {
    return completion(.internalServerError)
  }
  UserInstruction.Persistence.delete(id, from: database) { error in
    return completion(error as? RequestError)
  }
}
