import Foundation
import CouchDB
import LoggerAPI

extension UserInstruction {

  class Persistence {

    static func getAll(from database: Database, callback:
    @escaping (_ instructions: [UserInstruction]?, _ error: Error?) -> Void) {

      database.retrieveAll(includeDocuments: true) { documents, error in
        guard let documents = documents else {
          Log.error("Error retrieving all documents: \(String(describing: error))")
          return callback(nil, error)
        }

        let instructions = documents.decodeDocuments(ofType: UserInstruction.self)
        callback(instructions, nil)
      }
    }

    static func save(_ instruction: UserInstruction, to database: Database, callback:
    @escaping (_ instruction: UserInstruction?, _ error: Error?) -> Void) {

      database.create(instruction) { document, error in
        guard let document = document else {
          Log.error("Error creating new document: \(String(describing: error))")
          return callback(nil, error)
        }
        database.retrieve(document.id, callback: callback)
      }
    }

    static func delete(_ instructionID: String, from database: Database, callback:
    @escaping (_ error: Error?) -> Void) {

      database.retrieve(instructionID) { (instruction: UserInstruction?, error: CouchDBError?) in
        guard let instruction = instruction, let instructionRev = instruction._rev else {
          Log.error("Error retrieving document: \(String(describing:error))")
          return callback(error)
        }

        database.delete(instructionID, rev: instructionRev, callback: callback)
      }
    }
  }
}
